# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sphericalTest.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1074, 815)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btnRun = QtWidgets.QPushButton(self.centralwidget)
        self.btnRun.setGeometry(QtCore.QRect(940, 10, 111, 111))
        self.btnRun.setObjectName("btnRun")
        self.tableView = QtWidgets.QTableView(self.centralwidget)
        self.tableView.setGeometry(QtCore.QRect(10, 130, 1041, 631))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tableView.sizePolicy().hasHeightForWidth())
        self.tableView.setSizePolicy(sizePolicy)
        self.tableView.setSortingEnabled(True)
        self.tableView.setObjectName("tableView")
        self.lineConnectionString = QtWidgets.QLineEdit(self.centralwidget)
        self.lineConnectionString.setGeometry(QtCore.QRect(10, 10, 921, 31))
        self.lineConnectionString.setObjectName("lineConnectionString")
        self.plainTextQuery = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextQuery.setGeometry(QtCore.QRect(10, 50, 921, 71))
        self.plainTextQuery.setObjectName("plainTextQuery")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1074, 25))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Spherical. Connect to SqlLite DB"))
        self.btnRun.setText(_translate("MainWindow", "Run"))
        self.lineConnectionString.setText(_translate("MainWindow", "Northwind_small.sqlite"))
        self.plainTextQuery.setPlainText(_translate("MainWindow", "SELECT * FROM sqlite_master WHERE type=\'table\';"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

