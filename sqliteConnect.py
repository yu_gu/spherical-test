from sqlite3 import dbapi2 as sqlite3
import testGui as gui
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QAbstractTableModel
from PyQt5.QtWidgets import QMessageBox
import operator
import sys


class SqliteDB:
    def __init__(self):
        # Number of records to be retrieved from DB to View
        self.row_num = 10500

        self.db_already_inmemory = False
        self.db = None
        self.cursor = None
        self.data_base = ':memory:'
        self.view_column_names = []
        self.all_data_fetched = False

    # Connect ot DB and close previous connection and clear memory if needed
    def get_db(self, data_base):
        self.data_base = data_base

        if data_base == ':memory:':
            if not self.db_already_inmemory:
                self.db = sqlite3.connect(data_base)
                self.db_already_inmemory = True
        else:
            try:
                self.db.close()
                self.db = None
                self.cursor = None
            except (AttributeError, sqlite3.OperationalError, sqlite3.Warning):
                pass
            self.db = sqlite3.connect(data_base)
            self.db_already_inmemory = False

        self.db.row_factory = sqlite3.Row
        return self.db

    # Execute DB query, return [column names, data set]
    def query_db(self, query):
        self.all_data_fetched = False

        if query.strip().lower()[:6] == 'select':
            try:
                self.cursor = self.db.execute(query)
                self.view_column_names = [desc[0] for desc in self.cursor.description]
                view_data = self.cursor.fetchmany(self.row_num)
                if len(view_data) > 0:
                    return [self.view_column_names, view_data]
                else:
                    return None
            except (sqlite3.OperationalError, sqlite3.Warning) as e:
                self.all_data_fetched = True
                show_message(MainWindow, str(e))
        else:
            try:
                self.cursor = self.db.cursor().executescript(query)
                self.db.commit()
            except (sqlite3.OperationalError, sqlite3.Warning) as e:
                show_message(MainWindow, str(e))
        return None

    def fetch_more_db_data(self):
        # vertical_scroll_position = ui.tableView.verticalScrollBar().value()
        # print(vertical_scroll_position)
        # if vertical_scroll_position > self.row_num / 2:
        try:
            view_data = self.cursor.fetchmany(self.row_num)
            if not view_data:
                self.all_data_fetched = True
            return view_data
        except (sqlite3.OperationalError, sqlite3.Warning) as e:
            show_message(MainWindow, str(e))
        return []


def show_message(self, message):
    QMessageBox.about(self, "Error", message)


# Table data model for QTableView=tableView
class TableModel(QAbstractTableModel):

    def __init__(self, parent, view_data, header, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.view_data = view_data
        self.header = header

    def rowCount(self, parent):
        return len(self.view_data)

    def columnCount(self, parent):
        try:
            self.view_data[0]
        except IndexError:
            return 0
        return len(self.view_data[0])

    def data(self, index, role):

        if not index.isValid():
            return None

        row = index.row()
        column = index.column()

        if row > len(self.view_data):
            return None
        if column > len(self.view_data[row]):
            return None

        if role == QtCore.Qt.DisplayRole:
            return self.view_data[row][column]

        return None

    def canFetchMore(self, index=QtCore.QModelIndex()):
        if db.all_data_fetched is not True:
            return True
        return False

    def fetchMore(self, index=QtCore.QModelIndex()):
        row_count = self.rowCount(QtCore.QModelIndex())

        fresh_data = db.fetch_more_db_data()

        if fresh_data:
            self.view_data += fresh_data
            self.beginInsertRows(QtCore.QModelIndex(), row_count, len(self.view_data)-1)
            self.endInsertRows()

        return None

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.header[section]
        elif orientation == QtCore.Qt.Vertical and role == QtCore.Qt.DisplayRole:
            return section
        return None

    def sort(self, column, order):
        if column != 0:
            self.layoutAboutToBeChanged.emit()
            self.view_data = sorted(self.view_data, key=operator.itemgetter(column))
            if order == QtCore.Qt.DescendingOrder:
                self.view_data.reverse()
            self.layoutChanged.emit()


# Wrapper for PushButtonClicked event
def run():
    global db
    global table_model

    # Clearing previous TableModel Obj from memory, if any
    try:
        ui.tableView.setModel(TableModel(MainWindow,
                                         view_data=[],
                                         header=[]))
        table_model.view_data = None
        table_model = None
        query_result = None
    except (NameError, AttributeError):
        pass

    # Connect to DB, run query and flush tableView with data
    db.get_db(data_base=ui.lineConnectionString.text())
    query_result = db.query_db(query=ui.plainTextQuery.toPlainText())
    if query_result is not None:
        table_model = TableModel(MainWindow,
                                 view_data=query_result[1],
                                 header=query_result[0])
        ui.tableView.setModel(table_model)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()

    ui = gui.Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()

    db = SqliteDB()

    ui.btnRun.clicked.connect(run)

    # ui.tableView.verticalScrollBar().valueChanged.connect(db.fetch_more_db_data)

    app.exec_()
